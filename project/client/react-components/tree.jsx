
import ReactComponentBase from 'z-abs-corelayer-client/client/react-component/react-component-base';
import React from 'react';


export default class Tree extends ReactComponentBase {
  constructor(props) {
    super(props);
  }
  
  shouldUpdate(nextProps, nextState) {
    return !this.shallowCompare(this.props.id, nextProps.id)
      || !this.shallowCompare(this.props.current, nextProps.current)
      || !this.shallowCompare(this.props.expandCurrentFolder, nextProps.expandCurrentFolder)
      || !this.shallowCompare(this.props.activationFilter, nextProps.activationFilter)
      || !this.shallowCompare(this.props.allowExpand, nextProps.allowExpand)
      || !this.shallowCompare(this.props.project, nextProps.project)
      || !this.shallowCompare(this.props.darkMode, nextProps.darkMode);
  }
  
  renderFileIconClick(element, e) {
    if(1 === e.detail) {
      this.props.onClickFile(element.key, element.title, element.data.path, element.data.type);
    }
  }
  
  renderFileIcon(element) {
    let fileIcon = this.props.fileIcons.get(element.data.type);
    if(!fileIcon) {
      fileIcon = this.props.fileIcons.get('ukn');
    }
    const icon = element.data.valid ? fileIcon.valid : fileIcon.invalid;
    return (
      <img src={icon} className={this.theme(this.props.darkMode, "tree_icon")} alt=""
        onClick={this.renderFileIconClick.bind(this, element)}
      />
    );
  }
  
  renderFileTitleClick(element, e) {
    if(1 === e.detail) {
      this.props.onClickFile(element.key, element.title, element.data.path, element.data.type);
    }
  }
  
  renderFileTitle(element) {
    return (
      <span className={this.theme(this.props.darkMode, "tree_title")}
        onClick={this.renderFileTitleClick.bind(this, element)}
      >{element.title}</span>
    );
  }
  
  renderFile(element) {
    return (
      <li key={element.key} className={this.theme(this.props.darkMode, "tree_file")} role="treeitem" aria-selected={element.selected}>
        {this.renderExpander(element)}
        {this.renderFileIcon(element)}
        {this.renderFileTitle(element)}
      </li>
    );
  }
  
  renderExpander(element) {
    if(!element.children || 0 === element.children.length) {
      return (
        <span className={this.theme(this.props.darkMode, "tree_expander")}></span>
      );
    }
    else {
      const style = {style: element.expanded ? {transform:'scale(1.5) rotate(45deg)'} : {transform:'scale(1.5)'}};
      return (
        <span role="button" className={this.theme(this.props.darkMode, "tree_expander")} {...style}
          onClick={(e) => {
            this.props.onToggleFolder(element.key, !element.expanded);
          }}
          >&#9656;
        </span>
      );
    }
  }
  
  renderFolderIconClick(element, e) {
    if(1 === e.detail) {
      this.props.onClickFolder(element.key, element.title, element.data);
    }
  }
  
  renderFolderIcon(element, events) {
    let iconId = '';
    if('project_folder' === element.data.type) {
      if(element.data.notFound) {
        iconId = 'actorjs-project-not-found';
      }
      else {
        iconId = 'actorjs-project';
      }
    }
    else if('static_folder' === element.data.type) {
      iconId = 'actorjs-static';
    }
    else {
      if(0 === element.data.types.length) {
        iconId = 'actorjs-unknown';
      }
      else if(1 === element.data.types.length) {
        iconId = element.data.types[0];
      }
      else {
        iconId = 'actorjs-multi';
      }
    }
    let folderIcon = this.props.folderIcons.get(iconId);
    if(!folderIcon) {
      folderIcon = this.props.folderIcons.get('actorjs-unknown');
    }
    const icon = element.expanded ? folderIcon.open : folderIcon.closed;
    if(events) {
      return (
        <img src={icon} className={this.theme(this.props.darkMode, "tree_icon")} alt=""
          onClick={this.renderFolderIconClick.bind(this, element)}
        />
      );
    }
    else {
      return (
        <img src={icon} className={this.theme(this.props.darkMode, "tree_icon")} alt="" />
      );
    }
  }
  
  renderFolderTitleClick(element, e) {
    if(1 === e.detail) {
      this.props.onClickFolder(element.key, element.title, element.data);
    }
    else if(2 === e.detail) {
      this.props.onToggleFolder(element.key, !element.expanded);
    }
  }
  
  renderFolderTitle(element, events) {
    if(events) {
      return (
        <span className={this.theme(this.props.darkMode, "tree_title")}
          onClick={this.renderFolderTitleClick.bind(this, element)}
        >{element.title}</span>
      );
    }
    else {
      return (
        <span className={this.theme(this.props.darkMode, "tree_title")}>{element.title}</span>
      );
    }
  }
  
  renderFolder(element) {
    return (
      <li key={element.key} className={this.theme(this.props.darkMode, "tree_folder")} role="treeitem" aria-expanded={element.expanded} aria-selected={element.selected}>
        {this.renderExpander(element)}
        {this.renderFolderIcon(element, true)}
        {this.renderFolderTitle(element, true)}
        {this.renderFolders(element)}
      </li>
    );
  }
  
  renderProjectNotFound(element) {
    return (
      <li key={element.key} className={this.theme(this.props.darkMode, "tree_folder")} role="treeitem" aria-expanded={false} aria-selected={false}>
        {this.renderExpander(element)}
        {this.renderFolderIcon({data: { type: 'project_folder', notFound: true}}, false)}
        {this.renderFolderTitle(element, false)}
      </li>
    );
  }
     
  renderFolders(element) {
    if(!element.expanded) {
      return null;
    }
    const inner = element.children?.map((element) => {
      if(element.folder) {
        return this.renderFolder(element);
      }
      else {
        return this.renderFile(element);
      }
    });
    return (
      <ul key={element.key} className={this.theme(this.props.darkMode, "tree_folder")} role="group">
        {inner}
      </ul>
    );
  }
  
  render() {
    if(!this.props.project || !this.props.project.source || 0 === this.props.project.source.length) {
      return null;
    }
    const element = this.props.project.source[0];
    if(this.props.project.source[0].key) {
      if(this.props.expandCurrentFolder) {
        if(this.props.current.folder) {
          this.props.project.expandPath(this.props.current.folder.data.path + '/' + this.props.current.folder.title);
        }
      }
      return (
        <div id={this.props.id}>
          <ul key={element.key} className={this.theme(this.props.darkMode, "tree_root")} role="group">
            {this.renderFolder(element)}
          </ul>
        </div>
      );
    }
    else {
      return (
        <div id={this.props.id}>
          <ul key={element.key} className={this.theme(this.props.darkMode, "tree_root")} role="group">
            {this.renderProjectNotFound(element)}
          </ul>
        </div>
       );
    }
  }
}


Tree.defaultProps = {
  allowExpand: true
};
